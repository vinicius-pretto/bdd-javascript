const moment = require('moment');

class TaskHelper {
  constructor(taskModel) {
    this.taskModel = taskModel;
  }

  async initMock(task) {
    await this.taskModel.sync();
    await this.taskModel.destroy({ where: {} });
    const taskCreated = await this.taskModel.create(task);
    return taskCreated.id;
  }

  toJSON(resultSet) {
    if (!resultSet) {
      return {};
    }
    const task = resultSet.toJSON();
    
    return {
      ...task,
      dateTime: moment(task.dateTime).toISOString()
    }
  }

  async findById(taskId) {
    const resultSet = await this.taskModel.findOne({ 
      where: { 
        id: taskId 
      }
    });
    return this.toJSON(resultSet);
  }
}

module.exports = TaskHelper
