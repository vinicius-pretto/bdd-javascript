const HttpStatus = require('http-status');
const taskMock = require('../mocks/task.json');
const TaskHelper = require('./task.helper');

describe('Task routes', () => {
  let defaultTaskId;
  let taskHelper;

  before(() => {
    taskHelper = new TaskHelper(taskModel);
  });

  beforeEach(async () => {
    defaultTaskId = await taskHelper.initMock(taskMock.default);
  });

  context('POST /v1/tasks', () => {
    it('should create a new task', async () => {
      const response = await request
        .post('/v1/tasks')
        .send(taskMock.new);

      const taskCreated = await taskHelper.findById(response.body.id);
      const expectedTask = {...taskCreated, id: response.body.id}

      expect(response.status).to.be.eql(HttpStatus.CREATED);
      expect(taskCreated).to.be.eql(expectedTask);
    });
  });

  context('GET /v1/tasks', () => {
    it('should get all tasks', async () => {
      const response = await request.get('/v1/tasks');
      const expectedTasks = [
        {...taskMock.default, id: defaultTaskId}
      ];

      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(response.body.tasks).to.be.eql(expectedTasks);
    });
  });

  context('GET /v1/tasks/:taskId', () => {
    it('should get task by id', async () => {
      const response = await request.get(`/v1/tasks/${defaultTaskId}`);
      const expectedTask = {...taskMock.default, id: defaultTaskId};

      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(response.body).to.be.eql(expectedTask);
    });
  });

  context('UPDATE /v1/tasks/:taskId', () => {
    it('should update an existing task', async () => {
      const response = await request
        .put(`/v1/tasks/${defaultTaskId}`)
        .send(taskMock.updated);

      const taskUpdated = await taskHelper.findById(defaultTaskId);
      const expectedTask = {...taskMock.updated, id: defaultTaskId}
      
      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(taskUpdated).to.be.eql(expectedTask);
    });
  });

  context('DELETE /v1/tasks/:taskId', () => {
    it('should delete a task an existing task', async () => {
      const response = await request.delete(`/v1/tasks/${defaultTaskId}`);
      const taskDeleted = await taskHelper.findById(defaultTaskId);

      expect(response.status).to.be.eql(HttpStatus.OK);
      expect(taskDeleted).to.be.empty;
    });
  });
});
