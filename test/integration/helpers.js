process.env.NODE_ENV='test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const TaskModel = require('../../src/domain/tasks/task.model');

const Database = require('../../src/infrastructure/db/database');
const app = require('../../src/core/app');
const dbConnection = new Database().getConnection();

const taskModel = dbConnection.define('task', TaskModel)

chai.use(chaiHttp);

global.request = chai.request(app).keepOpen();
global.expect = chai.expect;
global.taskModel = taskModel;



