const TaskRepositoryFake = require('./task-repository.fake');
const TaskService = require('../../../src/domain/tasks/task.service');

describe('TaskService', () => {
  context('createTask()', () => {
    let taskService;
    let createTaskSpy;
    
    async function validateTask(task, errorMessage) {
      try {
        await taskService.createTask(task);
        throw Error('');
      } catch (error) {
        expect(error.message).to.be.eql(errorMessage);
      } finally {
        sinon.assert.notCalled(createTaskSpy);
      }
    }

    beforeEach(() => {
      taskService = new TaskService(TaskRepositoryFake);
      createTaskSpy = sinon.spy(TaskRepositoryFake, 'createTask');
    });

    afterEach(() => {
      createTaskSpy.restore();
    });

    it('should create a task', async () => {
      const task = {
        dateTime: '2018-11-18T17:09:13.643Z',
        description: 'Fazer o trabalho de Engenharia de Software II'
      }
      await taskService.createTask(task);

      sinon.assert.calledOnce(createTaskSpy);
      sinon.assert.calledWith(createTaskSpy, task);
    });

    context('Validation Error', () => {
      it('task doesn\'t have description', async () => {
        const task = {
          dateTime: '2018-11-18T17:09:13.643Z',
          description: ''
        }
        const expectedErrorMessage = '"description" is not allowed to be empty'; 
        validateTask(task, expectedErrorMessage);
      });
  
      it('task has less than three letters', async () => {
        const task = {
          dateTime: '2018-11-18T17:09:13.643Z',
          description: 'ab'
        }
        const expectedErrorMessage = '"description" length must be at least 3 characters long';
        validateTask(task, expectedErrorMessage);
      });
  
      it('task doesn\'t have dateTime', async () => {
        const task = {
          dateTime: '',
          description: 'Fazer o trabalho de Engenharia de Software II'
        }
        const expectedErrorMessage = '"dateTime" is not allowed to be empty';
        validateTask(task, expectedErrorMessage);
      });
    });

  });
});
