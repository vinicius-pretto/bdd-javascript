module.exports = {
  port: process.env.PORT || 3000,
  sequelize: {
    database: process.env.DB_NAME || 'bdd_javascript', 
    username: process.env.DB_USERNAME || 'vinicius',
    password: process.env.DB_PASSWORD || 'vinicius',
    options: {
      port: process.env.DB_PORT || 5432,
      host: process.env.DB_HOST || 'localhost',
      dialect: 'postgres',
      logging: false,
      define: {
        underscored: false,
        timestamps: false
      },
      operatorsAliases: false,
      pool: {
        max: process.env.DB_POOL || 5,
        min: 0,
        acquire: process.env.DB_ACQUIRE || 30000,
        idle: process.env.DB_IDLE || 10000
      }
    }
  } 
}
