const config = require('./config');
const app = require('./src/core/app');

async function bootstrap() {
  await app.listen(config.port);
  console.log(`Server is listening at port: ${config.port}`);
}

bootstrap();
