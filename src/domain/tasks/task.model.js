const Sequelize = require('sequelize');

const Task = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  dateTime: {
    type: Sequelize.DATE,
    field: 'date_time',
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  }
}

module.exports = Task;
