const HttpStatus = require('http-status');
const TaskRepository = require('./task.repository');
const TaskService = require('./task.service');

module.exports = (app, dbConnection) => {
  const taskRepository = new TaskRepository(dbConnection);
  const taskService = new TaskService(taskRepository);
  
  app.post('/v1/tasks', async (req, res) => {
    try {
      const task = await taskService.createTask(req.body);
      res.status(HttpStatus.CREATED).json(task);
    } catch (error) {
      console.log(error.message);
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  app.get('/v1/tasks', async (req, res) => {
    try {
      const tasks = await taskService.findAllTasks();
      res.json({ tasks: tasks });
    } catch (error) {
      console.log(error.message);
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  app.get('/v1/tasks/:taskId', async (req, res) => {
    try {
      const task = await taskService.findById(req.params.taskId);
      res.json(task);
    } catch (error) {
      console.log(error.message);
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  app.put('/v1/tasks/:id', async (req, res) => {
    try {
      await taskService.updateTask(req.params.id, req.body);
      res.sendStatus(HttpStatus.OK);
    } catch (error) {
      console.log(error.message);
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  app.delete('/v1/tasks/:id', async (req, res) => {
    try {
      await taskService.deleteTask(req.params.id);
      res.sendStatus(HttpStatus.OK);
    } catch (error) {
      console.log(error.message);
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });
}
