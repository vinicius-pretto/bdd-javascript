const moment = require('moment');
const TaskModel = require('./task.model');

class TaskRepository {
  constructor(dbConnection) {
    this.dbConnection = dbConnection;
    this.defineModel();
  }

  defineModel() {
    this.taskModel = this.dbConnection.define('task', TaskModel);
  }

  buildTask(resultSet) {
    if (!resultSet) {
      return {};
    }
    const task = resultSet.toJSON();
    
    return {
      ...task,
      dateTime: moment(task.dateTime).toISOString()
    }
  }

  async createTask(task) {
    await this.taskModel.sync();
    const resultSet = await this.taskModel.create(task);
    return this.buildTask(resultSet);
  }

  async findAllTasks() {
    await this.taskModel.sync();
    const resultSet = await this.taskModel.findAll();
    const tasks = resultSet.map(this.buildTask);
    return tasks;
  }

  async findById(taskId) {
    await this.taskModel.sync();
    const task = await this.taskModel.findOne({ 
      where: { 
        id: taskId 
      } 
    });
    return this.buildTask(task);
  }

  async updateTask(taskId, task) {
    await this.taskModel.sync();
    await this.taskModel.update(task, {
      where: {
        id: taskId
      }
    });
  }

  async deleteTask(taskId) {
    await this.taskModel.sync();
    await this.taskModel.destroy({
      where: {
        id: taskId
      }
    });
  }
}

module.exports = TaskRepository;
