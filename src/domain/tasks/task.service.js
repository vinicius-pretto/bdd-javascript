const Joi = require('joi');

const taskSchema = {
  dateTime: Joi.string().required(),
  description: Joi.string().min(3).required()
}

class TaskService {
  constructor(taskRepository) {
    this.taskRepository = taskRepository;
  }

  async createTask(task) {
    const validation = Joi.validate(task, taskSchema);

    if (validation.error) {
      const errorMessage = validation.error.details[0].message;
      throw Error(errorMessage);
    }
    const taskCreated = await this.taskRepository.createTask(task);
    return taskCreated;
  }

  async findById(taskId) {
    const task = await this.taskRepository.findById(taskId);
    return task;
  }

  async findAllTasks() {
    const tasks = await this.taskRepository.findAllTasks();
    return tasks;
  }

  async updateTask(taskId, task) {
    await this.taskRepository.updateTask(taskId, task);
  }

  async deleteTask(taskId) {
    await this.taskRepository.deleteTask(taskId);
  }
}

module.exports = TaskService;
