const { resolve } = require('path')
const config = require(resolve('config'))
const Sequelize = require('sequelize')

class Database {
  constructor() {
    this.connection = null
  }

  getConnection() {
    if (!this.connection) {
      this.connection = new Sequelize(
        config.sequelize.database,
        config.sequelize.username,
        config.sequelize.password,
        config.sequelize.options
      )
    }
    return this.connection
  }
}

module.exports = Database
