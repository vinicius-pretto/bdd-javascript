module.exports = (app, dbConnection) => {
  require('../domain/tasks/task.routes')(app, dbConnection);
}
