const express = require('express');
const bodyParser = require('body-parser');
const Database = require('../infrastructure/db/database');
const database = new Database();

const app = express();

app.use(bodyParser.json());

require('./router')(app, database.getConnection());

module.exports = app;
