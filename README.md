# BDD Javascript - Engenharia de Software II 
## Repositório para demonstrar o uso do Cucumber.js

### Requisitos

* [Node.js >= 9.6.1](https://nodejs.org)
* [Postgres](https://www.postgresql.org/)

### Iniciar a aplicação

#### Download das dependências

```
$ npm install
```

#### Iniciar o servidor

```
$ npm start
```

